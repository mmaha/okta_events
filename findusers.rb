#!/usr/bin/env ruby
require 'rubygems'
require 'json'
require 'rest-client'
require 'optparse'
require 'link_header'
require 'pp'

config_file = File.read('config.json')
config = JSON.parse(config_file)
status = ARGV[0] || "ACTIVE"
limit = 50

$orgurl = config["org_baseurl"]
$api_key = config["api_key"]

def findusers(next_url, status)
	data = RestClient.get next_url,
		{ 'Authorization' => $api_key ,:content_type => :json, :accept => :json}
	next_url = LinkHeader.parse(data.raw_headers["link"].join(",")).find_link(["rel", "next"]).to_a[0]
	return data, next_url
end

def deactivate(id)
	data = RestClient.post $orgurl + "users/" + id + "/lifecycle/deactivate",
		{ 'Authorization' => $api_key ,:content_type => :json, :accept => :json}
end

def process_data(data)
	data = JSON.parse( data, {:symbolize_names => true} )
	data.each do |item|
		puts [ item[:id], item[:profile][:login] ].join(",")
	end
end

# status eq "STAGED"	Users that have a status of STAGED
# status eq "PROVISIONED"	Users that have a status of PROVISIONED
# status eq "ACTIVE"	Users that have a status of ACTIVE
# status eq "RECOVERY"	Users that have a status of RECOVERY
# status eq "PASSWORD_EXPIRED"	Users that have a status of PASSWORD_EXPIRED
# status eq "LOCKED_OUT"	Users that have a status of LOCKED_OUT
# status eq "DEPROVISIONED"	Users that have a status of DEPROVISIONED
$next_url = URI.escape($orgurl + "users?filter=status eq \"" + status + "\"&limit=" + limit.to_s)

while ($next_url)
	data, $next_url = findusers($next_url, status)
	process_data(data)
end
