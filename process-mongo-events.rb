#!/usr/bin/env ruby
require 'rubygems'
require 'json'
require 'rest-client'
require 'optparse'
require "link_header"
require 'mongo'
require 'pp'


config_file = File.read('config.json')
config = JSON.parse(config_file)

mongohq_url=config["mongohq_url"]
mongo_coll=config['mongo_coll']

def get_connection(db_url)
  return @db_connection if @db_connection
  db = URI.parse(db_url)
  db_name = db.path.gsub(/^\//, '')
  puts db_name
  @db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
  @db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.password.nil?)
  @db_connection
end

def process(event)
  pp event["action"]["message"]
end

# db = get_connection
db = get_connection(mongohq_url)

#
# puts "Collections"
# puts "==========="
# collections = db.collection_names
# puts collections

# last_collection = collections[-1]
# coll = db.collection(last_collection)
coll = db.collection(mongo_coll)

# puts "Collection name: " + coll.name

# just show 5
# docs = coll.find().limit(15)

puts "\nEvents in #{coll.name}: #{coll.count}"
puts "=========================="


# coll.find.each { |row| puts row.inspect }

# next_document returns nil if there are no more objects that match
cursor = coll.find()
obj = cursor.next_document
while obj
  process obj

  coll.remove( { "_id" => obj["_id"] } )
  puts "Deleted ID:" + obj["_id"].to_s + "; Events remaining in #{coll.name}: #{coll.count}"
  obj = cursor.next_document
end
