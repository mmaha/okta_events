#!/usr/bin/env ruby
require 'rubygems'
require 'json'
require 'rest-client'
require 'optparse'
require "link_header"
require 'mongo'
require 'pp'

config_file = File.read('config.json')
config = JSON.parse(config_file)

orgurl = config["orgurl"]
api_key = config["api_key"]
mongohq_url=config["mongohq_url"]
mongo_coll=config['mongo_coll']

def get_connection(db_url)
  return @db_connection if @db_connection
  db = URI.parse(db_url)
  db_name = db.path.gsub(/^\//, '')
  puts db_name
  @db_connection = Mongo::Connection.new(db.host, db.port).db(db_name)
  @db_connection.authenticate(db.user, db.password) unless (db.user.nil? || db.password.nil?)
  @db_connection
end


db = get_connection(mongohq_url)

# puts "Collections"
# puts "==========="
# collections = db.collection_names
# puts collections
#
# last_collection = collections[-1]
coll = db.collection(mongo_coll)

puts "Collection name: " + coll.name

# just show 5
# docs = coll.find().limit(15)

puts "\nDocuments in #{coll.name}: #{coll.count}"

# docs = coll.find()
# docs.each{ |doc| puts doc.to_json }

def insert_data(coll, data)
  # if there is some data then go ahead and update the db
  unless (data.nil? || data.empty?)
    begin
      post_id = coll.insert(data)
      puts "Inserted #{post_id}"
    rescue Mongo::OperationFailure => e
      if e.message =~ /^11000/
        puts "Duplicate key error #{$!}"
        # Ignore and move on to the next record.
      else
        raise e
      end
    end
  end

  # data.each do |item|
  #   # puts item
  #  	puts [item[:eventId], item[:requestId], item[:published]].join(",")
  #   post_id = coll.insert(item)
  #   puts "Inserted #{post_id}"
  # end
end



while (orgurl.length > 0)
  puts "GET: " + orgurl
  data = RestClient.get orgurl, { 'Authorization' => api_key ,:content_type => :json, :accept => :json}
  # p "-------"
  # puts data.raw_headers["link"]
  # For some reason I get an array of the link objects. I parse it by joining it into a string.
  orgurl=LinkHeader.parse(data.raw_headers["link"].join(",")).find_link(["rel", "next"]).to_a[0]

  insert_data(coll, JSON.parse(data, {:symbolize_names => true} ) )

  if ( orgurl.nil? )
    puts "\n\n Did not get a next link, setting orgurl to self; Sleeping for 1 minute"
    sleep(60)
    orgurl=LinkHeader.parse(data.raw_headers["link"].join(",")).find_link(["rel", "self"]).to_a[0]
  end
end
