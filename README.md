Okta Events Processor
===

These set of scripts help with processing [Okta Events](http://developer.okta.com/docs/api/rest/events.html)

Description
---
- `get-events-from-okta.rb` gets the events from Okta and inserts them to MongoDB
- `process-mongo-events.rb` processes (currently just prints) and removes the event records from MongoDB
- `findusers.rb` will return a list of users based one of the status below:
    - STAGED, PROVISIONED, ACTIVE, RECOVERY, PASSWORD_EXPIRED, LOCKED_OUT, DEPROVISIONED
- `createUser.rb` helps create a user from the command line.

Requirements
---
- [ruby](https://www.ruby-lang.org/en/) 
- ruby libraries (see the source)

Instructions
---
- Rename `config.json.sample` to `config.json`
- Update the values in `config.json`
- Run `bundle install`
