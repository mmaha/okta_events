#!/usr/bin/env ruby
require 'rubygems'
require 'json'
require 'rest-client'
require 'optparse'

config_file = File.read('config.json')
config = JSON.parse(config_file)
status = ARGV[0] || "ACTIVE"
limit = 50

$orgurl = config["org_baseurl"] + "users" # + "users?activate=false"
$api_key = config["api_key"]


options = {}

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: getFromOkta.rb COMMAND [OPTIONS]"
   
   options[:verbose] = false
   opts.on( '-v', '--verbose', 'Output more information' ) do
     options[:verbose] = true
   end

   options[:login] = nil
   opts.on( '-u', '--user USERNAME', 'The login/username' ) do |value|
     options[:login] = value
   end
   
   options[:phone] = nil
   opts.on( '-n', '--phone Phone-number', 'Phone-number' ) do |value|
     options[:phone] = value
   end
   
   options[:password] = nil
   opts.on( '-p', '--password Password', 'Password' ) do |value|
     options[:password] = value
   end

   options[:email] = nil
   opts.on( '-e', '--email email', 'email address' ) do |value|
     options[:email] = value
   end
   
   options[:fname] = nil
   opts.on( '-f', '--firstname Firstname', 'The firstname' ) do |value|
     options[:fname] = value
   end

   options[:lname] = nil
   opts.on( '-l', '--lastname Lastname', 'The lastname' ) do |value|
     options[:lname] = value
   end

    # This displays the help screen, all programs are
   # assumed to have this option.
   opts.on( '-h', '--help', 'Help Screen' ) do
     puts opts
     exit
   end
 end

# opt_parser.parse!

begin
  opt_parser.parse!
  mandatory = [:login, :phone, :password, :email, :fname, :lname]  # Enforce the presence of various switches; http://stackoverflow.com/a/2149183
  missing = mandatory.select{ |param| options[param].nil? }         
  if not missing.empty?                                            
    puts "Missing options: #{missing.join(', ')}"                  
    puts optparse                                                  
    exit                                                           
  end                                                              
rescue OptionParser::InvalidOption, OptionParser::MissingArgument      #
  puts $!.to_s                                                           # Friendly output when parsing fails
  puts optparse                                                          #
  exit                                                                   #
end 

pobject = {
    :profile => {
        :firstName => options[:fname],
        :lastName => options[:lname],
        :email => options[:email],
        :login => options[:login],
        :mobilePhone => options[:phone]
    },
    :credentials => {
        :password => { :value => options[:password] }
    }
}

data = RestClient.post $orgurl, pobject.to_json, :Authorization => $api_key ,:content_type => :json, :accept => :json 
puts data if options[:verbose]


