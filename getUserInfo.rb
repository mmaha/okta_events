#!/usr/bin/env ruby
require 'rubygems'
require 'json'
require 'rest-client'
require 'optparse'
require 'link_header'
require 'pp'

config_file = File.read('config.json')
config = JSON.parse(config_file)
status = ARGV[0] || "ACTIVE"
limit = 50

$orgurl = config["org_baseurl"] + "users/"
$api_key = config["api_key"]

options = {}

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: getFromOkta.rb COMMAND [OPTIONS]"
   options[:verbose] = false
   opts.on( '-v', '--verbose', 'Output more information' ) do
     options[:verbose] = true
   end
    options[:user] = nil
   opts.on( '-u', '--user USERNAME', 'The login/username' ) do|user|
     options[:user] = user
   end
    # This displays the help screen, all programs are
   # assumed to have this option.
   opts.on( '-h', '--help', 'Help Screen' ) do
     puts opts
     exit
   end
 end

opt_parser.parse!

if options[:user].nil?
	abort("No username provided! Try \'" + __FILE__ + " --help\'") 
	puts opts
	exit
else
	user = options[:user] # Assign user with the command line option only if it is valid, if not error out
end

data = RestClient.get $orgurl + user, { 'Authorization' => $api_key ,:content_type => :json, :accept => :json}
data = JSON.parse(data, {:symbolize_names => true} )

pp data

id = data[:id]
puts user + ": " + id

data = RestClient.get $orgurl + id + "/groups", { 'Authorization' => $api_key ,:content_type => :json, :accept => :json}
data = JSON.parse(data, {:symbolize_names => true} )

data.each do |item|
	puts item[:id] + ":" + item[:profile][:name]
end


data = RestClient.get $orgurl + id + "/appLinks", { 'Authorization' => $api_key ,:content_type => :json, :accept => :json}
data = JSON.parse(data, {:symbolize_names => true} )

data.each do |item|
	puts "\"#{item[:label]}\"" + ":" + item[:linkUrl]
end

